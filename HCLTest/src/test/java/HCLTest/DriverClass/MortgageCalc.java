package HCLTest.DriverClass;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import HCLTest.utilites.Configurationprop;
import io.github.bonigarcia.wdm.WebDriverManager;

public class MortgageCalc {

	public Properties prop = null;
	public File file;
	public FileInputStream fis = null;
	public WebDriver driver;
	public boolean prope = false;

	By loanAmount = By.name("LOAN_AMOUNT");
	By term = By.xpath(".//select[@name='TERM']");
	By intrestAmount = By.name("INTEREST_RATE");
	By closePopup = By.xpath(".//span[contains(@class,'close')]");
	By totalPayment = By.xpath("//*[@id='KJE-C-GRAPH1']/div/table/tbody/tr/td[1]/h2");
	By monthlyPayment = By.id("KJE-MONTHLY_PAYMENT");
	By totalIntrest = By.xpath("//*[@id='KJE-C-GRAPH1']/div/table/tbody/tr/td[1]/h2/div");
	By Anually = By.id("KJE-BY_YEAR1");
	private XSSFWorkbook workbook;

	/*
	 * Method to load Properties file
	 */
	@BeforeSuite
	public boolean Loadprop() {
		try {

			file = new File(Configurationprop.propertiesfile);
			String abc = file.getPath();
			System.out.println(abc);
			File[] f = file.listFiles();

			for (File files : f) {
				if (files.getName().endsWith(".properties")) {

					fis = new FileInputStream(files);
					prop = new Properties();
					prop.load(fis);

				} else {

					System.out.println("Issue in properties file load");
				}
			}

		} catch (Exception e) {
			System.out.println("Error");
		}
		return prope;
	}

	/*
	 * Method to calculate Mortage with given values
	 */
	@Test
	public void mortageCalculation() throws IOException, InterruptedException {
		FileInputStream file = new FileInputStream(Configurationprop.ReadExcel);
		workbook = new XSSFWorkbook(file);
		XSSFSheet sheet1 = workbook.getSheet("MortgageInfo");
		XSSFSheet sheet2 = workbook.getSheet("PaymentInfo");

		XSSFRow row = sheet1.getRow(1);
		XSSFRow row1 = sheet2.getRow(1);

		driver.findElement(loanAmount).clear();
		driver.findElement(loanAmount).sendKeys(row.getCell(0).toString());

		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(closePopup));
		driver.findElement(closePopup).click();

		Select select = new Select(driver.findElement(term));
		select.selectByValue(row.getCell(1).toString().replace(".0", ""));

		driver.findElement(intrestAmount).clear();
		driver.findElement(intrestAmount).sendKeys(row.getCell(2).toString() + "%");

		Thread.sleep(3000);
		driver.findElement(Anually).click();

		String TotalPayment = driver.findElement(totalPayment).getText();
		String[] cc = TotalPayment.split(" ");
		String MonthlyPayments = driver.findElement(monthlyPayment).getText().replaceAll(",", "");
		String TotalIntrest = driver.findElement(totalIntrest).getText().substring(15).replaceAll(",", "");
		String TotalPayment1 = cc[2].trim();
		TotalPayment = TotalPayment1.replaceAll("\\n", "").replaceAll("[A-Za-z]", "").replaceAll(",", "");
		System.out.println(TotalPayment);

		Assert.assertEquals(MonthlyPayments, row1.getCell(0).toString());
		Assert.assertEquals(TotalIntrest, row1.getCell(1).toString());
		Assert.assertEquals(TotalPayment, row1.getCell(2).toString());

	}

	@BeforeTest
	public void browsersetup() {
		try {

			if (prop.getProperty("Browser").equalsIgnoreCase("Chrome")) {

				WebDriverManager.chromedriver().setup();
				driver = new ChromeDriver();

			} else if (prop.getProperty("Browser").equalsIgnoreCase("FF")) {

				WebDriverManager.firefoxdriver().setup();
				driver = new FirefoxDriver();
			}
		} catch (Exception e) {

		}
	}

	/*
	 * Method to Launch URL
	 */
	@BeforeClass
	public void url() {

		try {
			if (!(driver == null)) {
				driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
				driver.manage().window().maximize();
				driver.get(prop.getProperty("URL"));
			}

		} catch (Exception e) {

		}

	}

	/*
	 * Method to Close Browser
	 */
	@AfterClass
	public void tearDown() {
		driver.quit();
	}

}